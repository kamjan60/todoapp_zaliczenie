﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TodoApp_zaliczenie
{
    public static class TODOLIST
    {
        static List<string> Lista;
        static List<bool> ListaCheckow;
        public static List<string> getLista()
        {
            return Lista;
        }
        public static void setLista(List<string> lista)
        {
            Lista = lista;
        }

        public static List<bool> getListaCheckow()
        {
            return ListaCheckow;
        }
        public static void setListaCheckow(List<bool> listaCheckow)
        {
            ListaCheckow = listaCheckow;
        }
    }
}
