﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TodoApp_zaliczenie
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            if (TODOLIST.getLista() != null)
            {
                for (int i = 0; i < TODOLIST.getLista().Count(); i++)
                {
                    checkedListBox1.Items.Add(TODOLIST.getLista()[i]);
                }
                for (int j = 0; j < TODOLIST.getListaCheckow().Count(); j++)
                {
                    checkedListBox1.SetItemChecked(j, TODOLIST.getListaCheckow()[j]);
                }
            }
            if (checkedListBox1.Items.Count != 0)
            {
                button2.Enabled = true;
                button3.Enabled = true;
            }
            else 
            {
                button2.Enabled = false;
                button3.Enabled = false;
            }
        }

        private void infoToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void checkedListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (TODOLIST.getListaCheckow()[checkedListBox1.SelectedIndex] == false)
                {
                    TODOLIST.getListaCheckow()[checkedListBox1.SelectedIndex] = true;
                    checkedListBox1.SetItemChecked(checkedListBox1.SelectedIndex, true);
                }
                else
                {
                    TODOLIST.getListaCheckow()[checkedListBox1.SelectedIndex] = false;
                    checkedListBox1.SetItemChecked(checkedListBox1.SelectedIndex, false);
                }
            }
            catch (Exception x)
            {
                Console.WriteLine();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form2 f2 = new Form2();
            f2.ShowDialog();
        }

        private void zamknijAplikacjęToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.Application.Exit();
        }

        private void oProgramieToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Wersja - v1.1:\nProgram ten, ma na celu przechowywanie listy zadań dla użytkownika, z umożliwieniem ich dodawania, usuwania oraz edycji.", "O programie");
        }

        private void oAutorachToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Autorami są studenci III-ciego roku kierunku Informatyka:\n- Kamil Jankowski,\n- Szymon Kujawski", "O autorach");
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        public CheckedListBox getcheckedlistbox1()
        {
            return checkedListBox1;
        }
        public void setcheckedlistbox1(CheckedListBox checkedListBox1) {
            this.checkedListBox1 = checkedListBox1;
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            this.Hide();
            Delete f2 = new Delete();
            f2.ShowDialog();
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            Edit f2 = new Edit();
            f2.ShowDialog();
        }
    }
}
