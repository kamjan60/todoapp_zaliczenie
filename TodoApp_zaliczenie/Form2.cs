﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TodoApp_zaliczenie
{
    public partial class Form2 : Form
    {
        Form1 f1 = new Form1();
        public Form2()
        {
            InitializeComponent();
        }
        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            f1.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (richTextBox1.Text != "")
            {
                List<string> Lista;
                List<bool> ListaCheckow;
                if (TODOLIST.getLista() != null)
                {
                    Lista = TODOLIST.getLista();
                    ListaCheckow = TODOLIST.getListaCheckow();
                }
                else
                {
                    Lista = new List<string>();
                    ListaCheckow = new List<bool>();
                }
                Lista.Add(richTextBox1.Text);
                ListaCheckow.Add(false);
                TODOLIST.setLista(Lista);
                TODOLIST.setListaCheckow(ListaCheckow);
                this.Hide();
                f1.ShowDialog();
            }
            else
            {
                MessageBox.Show("Nie wprowadzono żadnej wiadomości!", "Błąd!");
            }
        }

        private void oProgramieToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Wersja - v1.1:\nProgram ten, ma na celu przechowywanie listy zadań dla użytkownika, z umożliwieniem ich dodawania, usuwania oraz edycji.", "O programie");
        }

        private void oAutorachToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Autorami są studenci III-ciego roku kierunku Informatyka:\n- Kamil Jankowski,\n- Szymon Kujawski", "O autorach");
        }

        private void zamknijAplikacjęToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.Application.Exit();
        }
    }
}
