﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TodoApp_zaliczenie
{
    public partial class Edit : Form
    {
        Form1 f1 = new Form1();
        public Edit()
        {
            InitializeComponent();

            if (TODOLIST.getLista() != null)
            {
                for (int i = 0; i < TODOLIST.getLista().Count(); i++)
                {
                    comboBox1.Items.Add(TODOLIST.getLista()[i]);
                }

            }
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            f1.ShowDialog();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (TODOLIST.getLista()[comboBox1.SelectedIndex] != richTextBox1.Text)
                {
                    TODOLIST.getLista()[comboBox1.SelectedIndex] = richTextBox1.Text;
                    MessageBox.Show("Zmiany zostały zapisane pomyślnie!", "Informacja");
                    this.Hide();
                    f1.ShowDialog();
                }
                else 
                {
                    MessageBox.Show("Nie dokonano żadnych zmian w zawartości zadania.", "Informacja");
                }
            }
            catch (Exception x)
            {
                MessageBox.Show("Wprowadzono błędny wybór!", "Błąd!");
            }
        }

        private void ChangeText(object sender, EventArgs e)
        {
            richTextBox1.Text = comboBox1.Text;
        }

        private void zamknijAplikacjęToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.Application.Exit();
        }

        private void oProgramieToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Wersja - v1.1:\nProgram ten, ma na celu przechowywanie listy zadań dla użytkownika, z umożliwieniem ich dodawania, usuwania oraz edycji.", "O programie");
        }

        private void oAutorachToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Autorami są studenci III-ciego roku kierunku Informatyka:\n- Kamil Jankowski,\n- Szymon Kujawski", "O autorach");
        }

        private void Edit_Load(object sender, EventArgs e)
        {
            comboBox1.SelectedIndex = 0;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
